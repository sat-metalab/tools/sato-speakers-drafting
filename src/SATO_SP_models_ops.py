"""
SATO speaker models operators

This module provides a few solutions for speaker distribution on a sphere:
- rings
- Fibonacci
- Sunflower distribution (based on the Golden Number)

When registered with Blender, this module provides an additional panel in the 3D View -> Panel
section (N key) with some buttons. It should be used with "SATO_speaker_distribution_models.blend"
file which contains the necessary models, materials and collections that are being used by this module.

It was developed mainly for internal used at the Société des Arts Technologiques [SAT]
"""

bl_info = {
    "name": "Speaker rings on a sphere",
    "author": "Michal Seta",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "description": "Adds a number of cube distributions on a sphere",
}


import bpy
import math
import numpy
from numpy import pi, cos, sin, arccos, arange
from bpy.types import Operator
from bpy.props import FloatVectorProperty
from bpy_extras.object_utils import AddObjectHelper, object_data_add
from bpy.props import IntProperty, StringProperty
from mathutils import Vector


speaker = bpy.data.objects['speaker']

context = bpy.context
assets_coll = bpy.data.collections["Assets"]
fib_coll = bpy.data.collections["Fibonacci"]
sun_coll = bpy.data.collections["sunflower"]
rings_coll = bpy.data.collections["rings"]

speaker = bpy.data.objects['speaker']


def ring(radius, n, z=0):
    points = []

    for i in range(n):
        theta = (math.pi*2) / n
        angle = theta * i

        x = math.cos(angle) * radius
        y = math.sin(angle) * radius

        points.append((x, y, z))

    return points


def make_ring(num, radius=19, z = 0):
    data = ring(radius, num, z)
    for loc in data:
        bpy.data.objects['speaker'].select_set(True)
        bpy.ops.object.duplicate()
        new_speaker = bpy.context.selected_objects[0]
        new_speaker.name = "ring"
        new_speaker.location = loc
        for slot in new_speaker.material_slots:
            slot.material = bpy.data.materials['Red']
        rings_coll.objects.link(new_speaker)
        assets_coll.objects.unlink(new_speaker)
        new_speaker.select_set(False)


def fibonacci_sphere(samples=1, radius_coeff=19):
    points = []
    phi = math.pi * (3. - math.sqrt(5.))  # golden angle in radians

    for i in range(samples):
        y = 1 - (i / float(samples - 1)) * 2  # y goes from 1 to -1
        radius = math.sqrt(1 - y * y)  # radius at y

        theta = phi * i  # golden angle increment

        x = math.cos(theta) * radius
        z = math.sin(theta) * radius

        points.append((x*radius_coeff, y*radius_coeff, z*radius_coeff))

    return points


def fibo(num):
    for loc in fibonacci_sphere(num):
        if loc[2] > -6:
            new_speaker = speaker.copy()
            new_speaker.name = "fibo"
            new_speaker.location = loc
            fib_coll.objects.link(new_speaker)


# sunflower
def sunflower_sphere(num):

    num_pts = num
    indices = arange(0, num_pts, dtype=float) + 0.5

    phi = arccos(1 - 2*indices/num_pts)
    theta = pi * (1 + 5**0.5) * indices

    x, y, z = cos(theta) * sin(phi), sin(theta) * sin(phi), cos(phi);
    return (x, y, z)


def sunflower(num):

    x, y, z = sunflower_sphere(num)
    for i in range(len(x)):
        xx = x[i] * 19
        yy = y[i] * 19
        zz = z[i] * 19
        if zz > -6:
            new_speaker = speaker.copy()
            new_speaker.name = "sunf"
            new_speaker.location = (xx, yy, zz)
            sun_coll.objects.link(new_speaker)


class MainPanel(bpy.types.Panel):
    bl_label = "SATO Speakers"
    bl_idname = "VIEW_PT_MainPanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'SATO speakers'

    def draw(self, context):
        layout = self.layout
        layout.scale_y = 1.2

        row = layout.row()
        row.label(text="Add speakers simulation", icon='OBJECT_ORIGIN')
        row = layout.row()
        row.operator("wm.add_sato_rings", icon='SPHERE', text="Rings")
        row = layout.row()


class SunflowerPanel(bpy.types.Panel):
    bl_label = "Sunflower"
    bl_idname = "VIEW_PT_SunPanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'SATO speakers'
    bl_parent_id = 'VIEW_PT_MainPanel'
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.label(text="Sunflower pattern", icon='DOT')
        row = layout.row()
        row.operator("wm.add_sato_sunflower", icon='SPHERE', text="Sunflower pattern")


class FiboPannel(bpy.types.Panel):
    bl_label = "Fibonacci"
    bl_idname = "VIEW_PT_FiboPanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'Sato Speakers'
    bl_parent_id = 'VIEW_PT_MainPanel'
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.label(text="Fibonacci pattern", icon='DOT')
        row = layout.row()
        row.operator("wm.add_sato_fibo", icon='SPHERE', text="Fibonacci pattern")


class WM_OT_add_sunflower(Operator):
    """Sunflower pattern of cubes on sphere"""
    bl_idname = "wm.add_sato_sunflower"
    bl_label = "Sunflower pattern of cubes on sphere"
    bl_options = {'REGISTER', 'UNDO'}

    total_num: IntProperty(
        name="Speaker count",
        description="Total number of speakers",
        min=0, max=256,
        default=196,
    )

    def execute(self, context):
        for o in sun_coll.objects:
            bpy.data.objects.remove(o)

        sunflower(self.total_num)
        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)


class WM_OT_add_fibo(Operator):
    """Fibonacci pattern of cubes on sphere"""
    bl_idname = "wm.add_sato_fibo"
    bl_label = "Fibonacci pattern of cubes on sphere"
    bl_options = {'REGISTER', 'UNDO'}

    total_num: IntProperty(
        name="Speaker count",
        description="Total number of speakers",
        min=0, max=256,
        default=196,
    )

    def execute(self, context):
        for o in fib_coll.objects:
            bpy.data.objects.remove(o)

        fibo(self.total_num)
        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)


class WM_OT_add_rings(Operator):
    """Add rings of cubes on sphere"""
    bl_idname = "wm.add_sato_rings"
    bl_label = "Add rings of cubes on sphere"
    bl_options = {'REGISTER', 'UNDO'}

    s_0: IntProperty(
        name="Ring 0",
        description="Number of speakers level 0",
        min=0, max=96,
        default=24,
    )

    s_1: IntProperty(
        name="Ring 1",
        description="Number of speakers level 1",
        min=0, max=96,
        default=24,
    )

    s_2: IntProperty(
        name="Ring 2",
        description="Number of speakers level 2",
        min=0, max=96,
        default=24,
    )

    s_3: IntProperty(
        name="Ring 3",
        description="Number of speakers level 3",
        min=0, max=96,
        default=24,
    )

    s_4: IntProperty(
        name="Ring 4",
        description="Number of speakers level 4",
        min=0, max=96,
        default=24,
    )

    s_5: IntProperty(
        name="Ring 5",
        description="Number of speakers level 5",
        min=0, max=96,
        default=24,
    )

    s_6: IntProperty(
        name="Ring 6",
        description="Number of speakers level 6",
        min=0, max=96,
        default=12,
    )

    def execute(self, context):
        for o in rings_coll.objects:
            bpy.data.objects.remove(o)

        make_ring(self.s_0, radius=18, z=-6)
        make_ring(self.s_1, radius=19, z=-1)
        make_ring(self.s_2, radius=18, z=5)
        make_ring(self.s_3, radius=17, z=9)
        make_ring(self.s_4, radius=14, z=13)
        make_ring(self.s_5, radius=10, z=16)
        make_ring(self.s_6, radius=6, z=18)
        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)


# Registration
def register():
    bpy.utils.register_class(MainPanel)
    bpy.utils.register_class(SunflowerPanel)
    bpy.utils.register_class(FiboPannel)
    bpy.utils.register_class(WM_OT_add_sunflower)
    bpy.utils.register_class(WM_OT_add_fibo)
    bpy.utils.register_class(WM_OT_add_rings)


def unregister():
    bpy.utils.unregister_class(MainPanel)
    bpy.utils.unregister_class(SunflowerPanel)
    bpy.utils.unregister_class(FiboPannel)
    bpy.utils.unregister_class(WM_OT_add_sunflower)
    bpy.utils.unregister_class(WM_OT_add_fibo)
    bpy.utils.unregister_class(WM_OT_add_rings)


if __name__ == "__main__":
    register()
